module Main where

import Protolude
import Data.Map.Strict ( (!?), mapKeysWith, filterWithKey, elems, insert, update, adjust, keys, fromList, singleton, lookup )
import Zero

import Network.Simple.TCP
import Data.ByteString as BS ( toStrict )

import Data.Aeson

default ([], Word, Text)

data Σ = Σ
   { tile :: Map Coord [Vase]
   , nest :: Map Team [Word]  -- 0 is empty
   , turn :: Team
   , hand :: Map Team Hand
   , opts :: Opts
   , echo :: Maybe Text
   , tids :: Map Text Word
   }

data Res = Res
   { res_win   :: Maybe Team
   , res_turn  :: Team
   , res_tile  :: Map Coord [Vase]
   , res_nest  :: Map Team [Word]
   , res_echo  :: Maybe Text
   , res_tids  :: Map Text Word
   , res_picks :: Maybe Focus
   , res_focus :: Maybe Focus
   } deriving Generic

instance ToJSON Res
instance ToJSON Hand
instance ToJSON Vase
instance ToJSON Focus

-- data Req = Req
--    { req_id :: Text
--    , req_msg :: Text
--    } deriving ( Generic )
--
-- instance ToJSON Req
--
-- instance FromJSON Req

type Coord = (Word,Word)

data Vase = Vase
   { team :: Team
   , rank :: Word
   } deriving ( Eq, Generic )

instance Ord Vase where
   compare = on compare rank

type Team = Word

data Hand = Hand
   { focus :: Focus  -- current focus
   , lnest :: Word  -- last recorded focus on nest
   , ltile :: Coord  -- last recorded focus on tile
   , picks :: Maybe Focus  -- grabbed vase origin
   } deriving Generic

data Focus = Nest Word | Grid Coord
   deriving Generic

data Opts = Opts
   { teams :: Word
   , scale :: Word
   , piles :: Word
   }

data Dir = N | S | E | W | X | J Coord

dir :: Text -> Maybe Dir
dir t
   | "N" <- t = Just N
   | "S" <- t = Just S
   | "E" <- t = Just E
   | "W" <- t = Just W
   | "X" <- t = Just X
-- | ["J",j] <- words t , Just coord <- fromText j = Just $ J coord
   | otherwise = Nothing

data Act = Pick | Undo | Play

new :: Σ
new = Σ
   { tile = fromList [ ((x,y),[]) | x <- genericTake s total , y <- genericTake s total ]
   , nest = fromList [ (n,genericReplicate p s) | n <- genericTake c total ]
   , turn = 0
   , hand = fromList [ (n,Hand { focus = Nest 0 , picks = Nothing , lnest = 0 , ltile = (prev $ div s 2,prev $ div s 2) }) | n <- genericTake c total ]
   , opts = Opts { teams = c , scale = s , piles = p }
   , echo = Nothing
   , tids = mempty
   }
   where
   c :: Word = 2  -- number of teams
   s :: Word = 4  -- size of square and number of vase ranks
   p :: Word = 3  -- number of piles in each player's nest

main :: IO ()
main = do
   print "* server start"
   serve (Host "127.0.0.1") "3003" $ \ (so,_) -> loop so new
-- game allows 4 state altering actions
   -- move Dir
   -- pick
   -- undo
   -- play

loop :: Socket -> Σ -> IO ()
loop so st
   | Just _ <- win = do
      send so $ BS.toStrict $ encode res
      loop so new  # "WIN"
   | otherwise = do
      send so $ BS.toStrict $ encode res
      req :: Maybe ByteString <- recv so 128
      case decodeUtf8 <$> req of
         Nothing -> loop so st  # "}!{ connection closed on empty req"
         Just msg -> loop so $ go $ words msg

   where

   -- process requests
   go :: [Text] -> Σ
   go (tid:act)
      | ["join"] <- act = st { tids = roll tid }
      | ["ping"] <- act = st
      | toEnum (length $ tids st) < teams (opts st) = st { echo = Just "wating for players..." }
      | Nothing <- lookup tid $ tids st = st
      | Just t <- lookup tid $ tids st , t /= turn st = st
      | ["move",x] <- act , Just d <- dir x = move d
      | ["pick"] <- act = pick
      | ["undo"] <- act = undo
      | ["play"] <- act = play
   go msg = st { echo = Just $ unwords $ "illegal msg:" : msg }

   -- calculated response
   res :: Res
   res = Res
      { res_win   = win
      , res_turn  = turn st
      , res_tile  = tile st
      , res_nest  = nest st
      , res_echo  = echo st
      , res_tids  = tids st
      , res_picks = picks =<< hand st !? turn st
      , res_focus = focus <$> hand st !? turn st
      }

   -- add client to team list
   roll :: Text -> Map Text Word
   roll tid
   -- | null $ tids st = singleton tid 0
      | toEnum (length $ tids st) < teams (opts st) = insert tid ix (tids st)
      | otherwise = tids st
      where
      ix = genericLength $ takeWhile identity $ zipWith (==) [0..prev $ teams $ opts st] (sort $ elems $ tids st)

   -- ACTIONS

   -- move hand
   move :: Dir -> Σ
   move d
      | Just h <- hand st !? turn st = st { hand = insert (turn st) (m h) (hand st) , echo = Nothing }
      | otherwise = st { echo = Just "error: no move" }
      where
      m :: Hand -> Hand
      m h
         | Nest n     <- focus h , E <- d = h { focus = Nest $ min (next n) (prev $ piles $ opts st) }
         | Nest n     <- focus h , W <- d = h { focus = Nest $ prev n }

         | Grid (x,y) <- focus h , N <- d = h { focus = Grid (x,prev y) }
         | Grid (x,y) <- focus h , S <- d = h { focus = Grid (x,min (next y) (prev $ scale $ opts st)) }
         | Grid (x,y) <- focus h , E <- d = h { focus = Grid (min (next x) (prev $ scale $ opts st),y) }
         | Grid (x,y) <- focus h , W <- d = h { focus = Grid (prev x,y) }

         | Nest n     <- focus h , X <- d = h { focus = Grid $ ltile h , lnest = n }
         | Grid c     <- focus h , X <- d = h { focus = Nest $ lnest h , ltile = c }

         | otherwise = h  -- no move

   -- pick vase to play
   pick :: Σ
   pick
      | Just h <- hand st !? turn st = st { hand = insert (turn st) (p h) (hand st) , echo = Nothing }
      | otherwise = st { echo = Just "error: no pick" }
      where
      p :: Hand -> Hand
      p h
         | Nest n <- focus h , Just (v:_) <- genericDrop n <$> nest st !? turn st , v > 0 = h { picks = Just (focus h) }
         | Grid c <- focus h , Just (v:_) <- tile st !? c , team v == turn st = h { picks = Just (focus h) }
         | otherwise = h  -- no pick

   -- drop grabbed vase
   undo :: Σ
   undo
      | Just h <- hand st !? turn st = st { hand = insert (turn st) (u h) (hand st) , echo = Nothing }
      | otherwise = st -- no undo
      where
      u :: Hand -> Hand
      u h = h { picks = Nothing }

   -- play vase if valid move
   play :: Σ
   play
      | Just h <- hand st !? turn st , Grid f <- focus h , Just p <- picks h , Nothing <- top (focus h) = st' f p
      | Just h <- hand st !? turn st , Grid f <- focus h , Just (Grid p) <- picks h , caps = st' f (Grid p)
      | Just h <- hand st !? turn st , Grid f <- focus h , Just (Nest p) <- picks h , caps , rescue = st' f (Nest p)
      | otherwise = st { echo = Just "no play" }
      where
      st' f p = st
         { tile = tile' f p
         , nest = nest' p
         , turn = mod (succ $ turn st) (teams $ opts st)
         , echo = Nothing
         , hand = map (\h -> h { picks = Nothing }) $ hand st
         }
      tile' f p
         | Grid c <- p = update (\x -> (: x) <$> top p) f $ adjust (drop 1) c $ tile st
         | Nest _ <- p = update (\x -> (: x) <$> top p) f $ tile st
      nest' p
         | Grid _ <- p = nest st
         | Nest n <- p = adjust (nth n prev) (turn st) $ nest st

   --

   -- can the vase be placed
   caps :: Bool
   caps
      | Just h <- hand st !? turn st , Nothing <- top (focus h) = True
      | Just h <- hand st !? turn st , Just GT <- compare <$> (top =<< picks h) <*> top (focus h) = True
      | otherwise = False

   -- check if rescue move is allowed
   rescue :: Bool
   rescue
      | Just h <- hand st !? turn st , f <- focus h , Grid (x,y) <- f , Just v <- top f , team v /= turn st = pred (scale $ opts st) ∈
         [ genericLength $ filter (\k@(kx,__) -> and @[] [x == kx,(team <$> top (Grid k)) == Just (team v)]) (keys $ tile st)
         , genericLength $ filter (\k@(__,ky) -> and @[] [y == ky,(team <$> top (Grid k)) == Just (team v)]) (keys $ tile st)
         , genericLength $ filter (\k@(kx,ky) -> and @[] [x == y,kx == ky,(team <$> top (Grid k)) == Just (team v)]) (keys $ tile st)
         , genericLength $ filter (\k@(kx,ky) -> and @[] [x == prev (scale (opts st) - y),kx == prev (scale (opts st) - ky),(team <$> top (Grid k)) == Just (team v)]) (keys $ tile st)
         ]
      | otherwise = False

   -- adjust nth element of list
   nth :: Word -> (a -> a) -> [a] -> [a]
   nth _ _ [] = []
   nth 0 f (x:xs) = f x : xs
   nth n f (x:xs) = x : nth (prev n) f xs

   -- get top vase
   top :: Focus -> Maybe Vase
   top f
      | Grid c <- f , Just (v:_) <- tile st !? c = Just v
      | Nest n <- f , Just (v:_) <- genericDrop n <$> nest st !? turn st , v > 0 = Just $ Vase { team = turn st , rank = v }
      | otherwise = Nothing

   -- top vase is own
   own :: Focus -> Bool
   own f = (team <$> top f) == Just (turn st)

   -- check if game is over
   win :: Maybe Team
   win
      | x:_ <- mapMaybe line (elems rows <> elems cols <> pure posd <> pure negd) = Just x  # show (elems rows,elems cols,posd,negd)
      | otherwise = Nothing
      where
      rows :: Map Word [Team]
      rows = map team <$> mapKeysWith (<>) fst (map (take 1) $ tile st)
      cols :: Map Word [Team]
      cols = map team <$> mapKeysWith (<>) snd (map (take 1) $ tile st)
      posd :: [Team]
      posd = join . elems $ map team . take 1 <$> filterWithKey (\(x,y) _ -> x == y) (tile st)
      negd :: [Team]
      negd = join . elems $ map team . take 1 <$> filterWithKey (\(x,y) _ -> x == prev (scale (opts st) - y)) (tile st)
      line :: [Team] -> Maybe Team
      line [] = Nothing
      line (x:xs)
      -- | False  # "line: " <> show (x:xs) = undefined
         | all (x ==) xs && genericLength (x:xs) == scale (opts st) = Just x
         | otherwise = Nothing
